<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'],function () {
    // Get All Lyrics
    Route::get('lyrics','App\Http\Controllers\v1\LyricController@getAllLyrics');
    // Create Lyric
    Route::post('lyrics','App\Http\Controllers\v1\LyricController@createLyric');
    Route::get('lyrics/search','App\Http\Controllers\v1\LyricController@searchLyric');
    // Update Lyric
    Route::put('lyrics/{lyricId}/edit','App\Http\Controllers\v1\LyricController@updateLyric');
    // Like Lyric
    Route::post('lyrics/like/{lyricId}/','App\Http\Controllers\v1\LyricController@likeLyric');
    // Get All Categories
    Route::get('categories','App\Http\Controllers\v1\categoryController@getAllCategories');

});