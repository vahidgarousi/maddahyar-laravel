php artisan cache:clear
php artisan config:clear
php artisan config:cache
php artisan key:generate
php artisan view:clear
php artisan migrate:reset
php artisan migrate
php artisan db:seed
pause