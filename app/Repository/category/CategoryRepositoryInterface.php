<?php


namespace App\Repository\category;


use Illuminate\Support\Collection;

interface CategoryRepositoryInterface
{
    public function all(): Collection;

    public function paginate($count = 10);
}