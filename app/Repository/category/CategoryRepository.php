<?php


namespace App\Repository\category;


use App\Http\Resources\CategoryCollection;
use App\Models\Category;
use App\Repository\BaseRepository;
use Illuminate\Support\Collection;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }


    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginate($count = 10)
    {
        return new CategoryCollection(Category::paginate($count));
    }

    public function lyrics()
    {
        return $this->model->lyrics();
    }
}