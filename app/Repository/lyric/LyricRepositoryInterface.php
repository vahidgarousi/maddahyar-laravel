<?php


namespace App\Repository\lyric;


use App\Models\Lyric;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface LyricRepositoryInterface
{
    public function all(): Collection;

    public function paginate($count = 10);

    public function likeLyric(Lyric $lyric): Lyric;

    public function updateLyric(Lyric $lyric, array $validated);

    public function search(Request $request);

    public function filter(Request $request, int $int);

}