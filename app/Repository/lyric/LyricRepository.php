<?php


namespace App\Repository\lyric;

use App\Http\Resources\LyricCollection;
use App\Http\Resources\Lyric as LyricResource;
use App\Models\Category;
use App\Models\Lyric;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Repository\BaseRepository;

class LyricRepository extends BaseRepository implements LyricRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param Lyric $model
     */
    public function __construct(Lyric $model)
    {
        parent::__construct($model);
    }


    public function all(): Collection
    {
        return $this->model->all();
    }

    public function paginate($count = 10)
    {
        return new LyricCollection(Lyric::with('categories')->paginate($count));
    }

    public function categories()
    {
        return $this->model->categories();
    }

    public function likeLyric(Lyric $lyric): Lyric
    {
        $lyric->like_count = $lyric->like_count + 1;
        $lyric->save();
        return $lyric;
    }

    public function updateLyric(Lyric $lyric, array $validated)
    {
        $lyric->update($validated);
        return new LyricResource($lyric);
    }

    /**
     * @param Request $request
     * @param int $count
     */
    public function filter(Request $request, int $count)
    {
        if (!$request->has('category_id')) {
            return $this->paginate($count);
        };
        $category = Category::find($request->input('category_id'));
        return new LyricCollection($category->lyrics()->paginate($count));
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
        return Lyric::query()
            ->where('title', 'LIKE', '%' . $keyword . '%')
            ->orWhere('content','LIKE','%' . $keyword . '%')
            ->get();
    }
}