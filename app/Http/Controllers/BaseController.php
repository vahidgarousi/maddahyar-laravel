<?php


namespace App\Http\Controllers;


use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response as HTTPResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class BaseController extends  Controller
{
    /**
     * Custom method to return response.
     *
     * @since 1.0.0
     *
     * @param string $status
     * @param mixed $data
     * @param int $code
     *
     * @return JsonResponse
     */
    public function response(string $status, $data, int $code)
    {
        return response()->json([
            'status' => $status,
            'data' => $data,
            'code' => $code,
        ], $code);
    }


    /**
     * Custom method to return error response.
     *
     * @since 1.0.0
     *
     * @param string $status
     * @param mixed $data
     * @param int $code
     *
     * @return JsonResponse
     */
    public function error(string $status, $data, $code)
    {
        if ($data instanceof ModelNotFoundException) {
            $code = HTTPResponse::HTTP_NOT_FOUND;
            $notFoundMessage = "Resource Not Found";
        }

        if (!($code >= 100 AND $code <= 511)) {
            $code = HTTPResponse::HTTP_INTERNAL_SERVER_ERROR;
            $data =  new Exception(
                $data->getMessage(),
                HTTPResponse::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        if ($code >= 500) {
            report($data);
            $dataMessage = "Something went wrong.";
        } else {
            $dataMessage = $notFoundMessage ?? $data->getMessage();
        }

        return response()->json([
            'status' => $status,
            'data' => $dataMessage,
            'code' => $code,
        ], $code);
    }

}