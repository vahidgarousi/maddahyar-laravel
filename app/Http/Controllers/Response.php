<?php


namespace App\Http\Controllers;
use Illuminate\Http\Response as HTTPResponse;


class Response extends HTTPResponse
{
    const ADDED = "Added";
    const REMOVED = "Removed";
    const ACCESS = "You don't have access.";
    const WRONG = "Entried data wasn't correct.";
    const ERROR = "Error";
    const SUCCESS = "Success";
    const UNAUTHORIZED = "Unauthorized";
    const NOTFOUND = "The item you're trying for is not found.";
    const PAGE_NOT_FOUND = "Page not found.";
    const INVALID_ROUTE = "Invalid route.";
}