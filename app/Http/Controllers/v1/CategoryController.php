<?php

namespace App\Http\Controllers\v1;

use App\Exam;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Response as HTTPResponse;
use App\Models\Lyric;
use App\Repository\category\CategoryRepositoryInterface;
use App\User;
use Exception;

class CategoryController extends BaseController
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAllCategories()
    {
        try {
            $data = $this->categoryRepository->paginate(10);
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }

    public function getLyrics(Lyric $lyric)
    {
        return $lyric->categories()->where('category_id', $lyric->id)->first();
    }
}
