<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\BaseController;
use App\Http\Requests\LyricRequest;
use App\Models\Lyric;
use App\Http\Resources\Lyric as LyricResource;
use App\Repository\lyric\LyricRepositoryInterface;
use App\Http\Controllers\Response as HTTPResponse;
use Exception;
use Illuminate\Http\Request;

class LyricController extends BaseController
{
    protected $lyricsRepository;

    public function __construct(LyricRepositoryInterface $lyricsRepository)
    {
        $this->lyricsRepository = $lyricsRepository;
    }

    public function getAllLyrics(Request $request)
    {
        try {
            $data = $this->lyricsRepository->filter($request, 10);
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }

    public function getLyric(Lyric $lyric)
    {
        try {
            $data = new LyricResource($lyric);
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }

    public function getAllCategories()
    {
        $data = $this->lyricsRepository->categories();
        return $this->response(
            HTTPResponse::SUCCESS,
            $data,
            HTTPResponse::HTTP_OK
        );
    }

    public function createLyric(LyricRequest $request)
    {
        $data = $this->lyricsRepository->create($request->validated());
        try {
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }

    public function likeLyric(Lyric $lyric)
    {
        $data = $this->lyricsRepository->likeLyric($lyric);
        try {
            return $this->response(
                HTTPResponse::SUCCESS,
                true,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }


    public function updateLyric(Lyric $lyric, LyricRequest $request)
    {
        $data = $this->lyricsRepository->updateLyric($lyric, $request->validated());
        try {
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }

    public function searchLyric(Request $request)
    {
        $data = $this->lyricsRepository->search($request);
        try {
            return $this->response(
                HTTPResponse::SUCCESS,
                $data,
                HTTPResponse::HTTP_OK
            );
        } catch (Exception $exception) {
            return $this->error(
                HTTPResponse::ERROR,
                $data,
                HTTPResponse::HTTP_OK
            );
        }
    }
}
