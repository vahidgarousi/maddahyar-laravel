<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use App\Http\Resources\Category as CategoryResource;

class CategoryCollection extends BaseResource
{

    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = CategoryResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'categories' => $this->collection,
            'meta' => $this->pagination,
        ];
    }
}
