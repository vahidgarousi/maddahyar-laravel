<?php

namespace App\Http\Resources;

use App\Http\Resources\Lyric as LyricResource;
use Illuminate\Http\Request;

class LyricCollection extends BaseResource
{

    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = LyricResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lyrics'       => $this->collection,
            'meta' => $this->pagination,
        ];
    }
}
