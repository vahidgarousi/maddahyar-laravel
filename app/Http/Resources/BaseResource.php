<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;

abstract class BaseResource extends ResourceCollection
{
    protected  $pagination;

    public function __construct($resource) {
        $this->pagination = [
            'itemsCount'        => $resource->count(),
            'totalPages'        => $resource->total(),
            'sizePerPage'     => $resource->perPage(),
            'currentPage' => $resource->currentPage(),
        ];
        $resource = $resource->getCollection();
        parent::__construct($resource);
    }

}