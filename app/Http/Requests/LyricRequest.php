<?php

namespace App\Http\Requests;


use App\Http\Requests\Api\FormRequest;

class LyricRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'content' => 'required|max:10000',
            'categoryId' => 'required',
        ];
    }

}
