<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lyric extends Model
{
    protected $fillable = ['title', 'content', 'like_count'];
    use HasFactory;

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
