<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLyric extends Model
{
    use HasFactory;
    public $timestamps = false;
}
