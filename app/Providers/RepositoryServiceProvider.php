<?php

namespace App\Providers;

use App\Repository\category\CategoryRepository;
use App\Repository\BaseRepository;
use App\Repository\category\CategoryRepositoryInterface;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\lyric\LyricRepository;
use App\Repository\lyric\LyricRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(LyricRepositoryInterface::class, LyricRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
