<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Ybazli\Faker\Facades\Faker;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => Faker::firstName(),
            'family' => Faker::lastName(),
            'mobile' => Faker::mobile(),
            'token' => $this->faker->uuid,
            'image' => 'https://ghonoot.com/upload/category/375263434-category.jpg'
        ];
    }
}
