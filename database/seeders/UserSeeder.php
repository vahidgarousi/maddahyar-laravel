<?php

namespace Database\Seeders;

use App\Models\LyricCategory;
use App\Models\User;
use Database\Factories\LyricFactory;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(500)->create();
    }
}
