<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Lyric;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()->count(100)->create();
        foreach (Category::all() as $category) {
            $lyrics = Lyric::all()->take(rand(1, 3))->pluck('id');
            $category->lyrics()->attach($lyrics);
        }
    }
}
