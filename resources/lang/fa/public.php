<?php
return [
    'comment' => [
        'successfully_created' => 'Your Comment Successfully added',
        'adding_error' => 'Unfortunately Your Comment can\'t be added',
    ],
    'user' => [
        'successfully_created' => 'User Successfully created, otp send',
        'already_exist_opt_send' => 'User already exist. otp send',
        'otp_expired_opt_send' => 'Otp expired . otp send',
        'please_active_your_account' => 'Otp expired . otp send',
        'does_not_exist' => 'User does not exist',
        'unauthorized' => 'Unauthorized',
        'registration_error' => 'Unfortunately a error occurred',
        'confirmation_successfully_finished' => 'you are successfully registered',
        'firebase_notification_successfully_registered_title' => 'Notification Enabled',
        'firebase_notification_successfully_registered_body' => 'Your notification enabled',
        'no_notifications' => 'You haven\'t any notifications',
        'too_many_requests_hourly' => 'your request attempts exceeded , please try :hour later',
    ],
    'role' => [
        'forbidden' => 'Forbidden request',
    ],
    'class' => [
        'successfully_created' => 'Successfully created',
        'forbidden' => 'already exist',
    ],
    'not_found' => 'یافت نشد',
];
